#include <stdio.h>
#include <Windows.h>
#include <Math.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	float a = 1000, b = 0.0001, res;

	res = (pow(a + b, 3) - pow(a, 3)) / (3 * a * b * b + b * b * b + 3 * a * a * b);
	printf("%f", res);

	return 0;
}